# -*- coding: utf-8 -*-

import json
import urllib3
import numpy as np
import pandas as pd
from pandas import DataFrame
import matplotlib.pyplot as plt

import tensorflow as tf
import tensorflow.contrib.rnn as rnn
import tensorflow.contrib.learn as tflearn
import tensorflow.contrib.layers as tflayers
import tensorflow.contrib.metrics as metrics
from tensorflow.contrib.learn.python.learn import learn_runner

#Url to an api should be granted here.
url = ''
http = urllib3.PoolManager()
r = http.request('GET', url)
getResponseDownloadBitrate = r.data

headersDownloadBitrate = json.loads(json.loads(getResponseDownloadBitrate.decode('utf8'))['header'])
resultsDownloadBitrate = json.loads(json.loads(getResponseDownloadBitrate.decode('utf8'))['rows'])

df = DataFrame(resultsDownloadBitrate)
df.columns = ["time_stamp","downlink_bitrate"]

ts = pd.Series(df.downlink_bitrate)
ts.index = df.time_stamp
ts.plot(c='b',title='DW')
plt.show()

#Converting data into smaller arrays to train and feed the recurrent neural network model
TS = np.array(ts)
num_periods = 48 
f_horizon = 1

x_data = TS[:(len(TS)-(len(TS) % num_periods))]
x_batches = x_data.reshape(-1,num_periods,1)

y_data = TS[1:(len(TS)-(len(TS) % num_periods))+f_horizon]
y_batches = y_data.reshape(-1,num_periods,1)

def test_data(series,forecast,num_periods):
    test_x_setup = TS[-(num_periods + forecast):]
    testX = test_x_setup[:num_periods].reshape(-1,num_periods,1)
    testY = TS[-(num_periods):].reshape(-1,num_periods,1)
    return testX, testY

X_test, Y_test = test_data(TS,f_horizon,num_periods)


#Creating the modeal
tf.reset_default_graph() 

num_periods = 48
inputs = 1
hidden = 400
output = 1

X = tf.placeholder(tf.float32,[None, num_periods, inputs]) 
Y = tf.placeholder(tf.float32,[None, num_periods, output])

basic_cell = tf.contrib.rnn.BasicRNNCell(num_units=hidden, activation=tf.nn.relu) 
rnn_output, states = tf.nn.dynamic_rnn(basic_cell, X, dtype=tf.float32) 

learning_rate = 0.001 

stacked_rnn_output = tf.reshape(rnn_output, [-1,hidden]) 
stacked_outputs = tf.layers.dense(stacked_rnn_output, output) 
outputs = tf.reshape(stacked_outputs,[-1,num_periods,output]) 

loss = tf.reduce_sum(tf.square(outputs-Y)) 
optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate) 
training_op = optimizer.minimize(loss) 

init = tf.global_variables_initializer() 

epochs = 2000

with tf.Session() as sess:
    init.run()
    for ep in range(epochs):
        sess.run(training_op, feed_dict={X: x_batches, Y: y_batches})
        if ep % 100 == 0:
            mse = loss.eval(feed_dict={X: x_batches, Y: y_batches})
            print(ep, "\tMSE", mse)
    
    y_pred = sess.run(outputs, feed_dict={X: X_test})
    print(y_pred)
    
#Data visualization
plt.plot(x_data,label="Observado")
plt.plot([None for i in x_data] + [x for x in pd.Series(np.ravel(y_pred))],label='Forecast') 
plt.legend()   
plt.grid()
plt.show()
